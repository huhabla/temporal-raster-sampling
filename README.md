# t.rast.sample

Sample a space time raster dataset at specific vector point coordinates and write the output into a file or stdout.
This is a PyGRASS implementation.

This module works only with the latest GRASS 7.1 trunk version.

## Installation:

1. Switch into the GRASS 7 trunk temporal directory that contains all
   temporal modules

2. Clone the repository:

        git clone https://huhabla@bitbucket.org/huhabla/temporal-raster-sampling.git t.rast.sample

3. Install the module and its manpage

        cd t.rast.sample
        make